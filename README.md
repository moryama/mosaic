# Mosaic

An interface to **play a language game** during **online lessons**.

The teacher controls the **widgets** and **drag-and-drop** components, while sharing the screen with the students. 

Objective of the game is to build a text combining the pieces (like a jigsaw) while strategically collecting points. 🤖 🏆

Check out the [**demo**](https://mosaic-language-game.herokuapp.com/) on Heroku (might take a few seconds to load).

## Features

- a **timer** to check players turn
- a score board to **assign or remove** points
- a 'box' to **randomly pick** pieces from
- a main area where the pieces can be **moved around**
- an area to **drag/drop** pieces for later usage
- a button to **save** the game status
- a **reset** button


## Tools

- **Framework:** [React 17.0.1](https://reactjs.org)

- **State management:** [Redux 4.0.5](https://redux.js.org), [react-redux](https://react-redux.js.org/)

- **Server for demo version:** [json-server](https://github.com/typicode/json-server)

- **Style:** [React-Bootstrap](https://react-bootstrap.github.io/)

- **Timer component:** [React Countdown Circle Timer](https://github.com/vydimitrov/react-countdown-circle-timer)

- **Drag and drop:**  [React DnD](https://react-dnd.github.io/react-dnd/about)


## Development notes

### **Drag and drop**

This is my first implementation of [drag](https://gitlab.com/moryama/mosaic/-/blob/master/client/src/components/Piece.js#L14) and [drop](https://gitlab.com/moryama/mosaic/-/blob/master/client/src/components/Container.js#L9). 

I decided to use the [React DnD](https://react-dnd.github.io/react-dnd/about) library. 

Another option was to use the more common [HTML drag-and-drop API](https://developer.mozilla.org/en-US/docs/Web/API/HTML_Drag_and_Drop_API). In hindsight, this option could have been more convenient for focusing more on learning the mechanism of dnd.

Generally speaking, a challenge with dnd is to have a **smooth landing effect** after drop. 

To achieve this I needed to identify the dropping [coordinates](https://gitlab.com/moryama/mosaic/-/blob/master/client/src/utils/coordinates.js) in relation to the drop element/area and its location on the page, to accomodate different screen sizes.

While the current result isn't perfect, it is working for the purpose of the app.

### **Deployment**

While working on this demo I was using [json-server](https://github.com/typicode/json-server) as an easy, hassle-free way to serve data. When it came time to deploy to Heroku I decided to stay with this tool. 

I took this chance to learn how to **quickly deploy a demo** with a combination of React + json-server.

I followed these steps:

- in the root directory I set a `client/` folder with my React app and a `demo-server/` folder with the simplest possible implementation of a `json-server` backend

- to be able to identify json-server, Heroku needs to find its `package.json` and `index.js` file in the root directory, so these files were moved to the `root`

- additionally, `json-server` needs to find a `public/` folder in the root directory containing the app `build/`. To do this, I added a [script]((https://gitlab.com/moryama/mosaic/-/blob/master/package.json#L9)) in `package.json` to run an Heroku postbuild action that moves the `build/` directory to root and renames it to `public/`.

These resources were of great help:

- Youssef Zidan on [Dev](https://dev.to/youssefzidan/deploying-fake-back-end-server-database-using-json-server-github-and-heroku-1lm4)
- Onur Iltan on [StackOverflow](https://stackoverflow.com/a/62752667/13628287)

## TODOs and beyond

- add **end-to-end tests**
- implement a version for a **single player** that **auto-checks** the piece combination
- implement a **custom timer component**

## License

No license is available at the moment.