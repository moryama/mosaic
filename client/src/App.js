import React from 'react';
import { useDispatch } from 'react-redux';
import { initializePieces } from './reducers/pieceReducer';
import './App.css';
import GameBoard from './components/GameBoard';
import BSContainer from 'react-bootstrap/Container';
import Toolbar from './components/Toolbar';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Footer from './components/Footer';


const App = () => {
  const dispatch = useDispatch();
  dispatch(initializePieces());

  return (
    <div id="page">
      <h1>Mosaic</h1>
      <BSContainer>
        <Row className="justify-content-md-center">
          <Col lg={3}>
            <Toolbar />
          </Col>
          <Col lg={9}>
            <GameBoard />
          </Col>
        </Row>
      </BSContainer>
      <Footer />
    </div>
  );
};


export default App;
