export const CONTAINER_NAMES = {
  DESK: 'desk',
  HANGER: 'hanger'
};

export const ITEM_TYPES = {
  PIECE: 'piece',
};
