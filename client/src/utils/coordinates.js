import { CONTAINER_NAMES } from '../constants';


const getDeskToPageLeft = () => {

  const pageDiv = document.getElementById('page');
  const deskDiv = document.getElementById('desk');

  const pageWidth = pageDiv.offsetWidth;
  const deskWidth = deskDiv.offsetWidth;

  const deskToPageLeftBorder = (pageWidth - deskWidth) / 2;

  return deskToPageLeftBorder;
};

const getDeskToPageTop = () => {
  const pageDiv = document.getElementById('page');
  const deskDiv = document.getElementById('desk');

  const pageHeight = pageDiv.offsetHeight;
  const deskHeight = deskDiv.offsetHeight;
  const deskToPageTopBorder = (pageHeight - deskHeight) / 2;

  return deskToPageTopBorder;
};

const getNew = (item, containerName, monitor) => {
  const { DESK, HANGER } = CONTAINER_NAMES;

  let left;
  let top;
  const itemIsChangingContainer = item.loc !== containerName;

  if (itemIsChangingContainer) {
    const prevTop = monitor.getClientOffset().y;
    const prevLeft = monitor.getClientOffset().x;

    if (containerName === DESK) {
      left = prevLeft - getDeskToPageLeft();
      top = prevTop - getDeskToPageTop();
    } else if (containerName === HANGER) {
      left = prevLeft - 1180;
      top = prevTop - getDeskToPageTop();
    } else {
      left = prevLeft;
    }
  } else {
    // moving inside a container
    const delta = monitor.getDifferenceFromInitialOffset();
    top = Math.round(item.top + delta.y);
    left = Math.round(item.left + delta.x);
  }

  return { top, left };
};


export default {
  getNew
};