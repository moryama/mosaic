import axios from 'axios';

const baseUrl = 'https://mosaic-language-game.herokuapp.com';

const getAll = async () => {
  const response = await axios.get(`${baseUrl}/gamePieces`);
  return response.data;
};

const update = async (id, newObject) => {
  const response = await axios.put(
    `${baseUrl}/gamePieces/${id}`,
    newObject
  );
  return response.data;
};

const getInitial = async () => {
  const response = await axios.get(`${baseUrl}/initialPieces`);
  return response.data;
};

export default {
  getAll,
  update,
  getInitial
};