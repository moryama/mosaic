const initialState = false;


const boxReducer = (state = initialState, action) => {
  switch (action.type) {
  case 'SET_TO_FULL':
    return true;
  case 'SET_TO_EMPTY':
    return false;
  default:
    return state;
  }
};

export const setBoxToFull = () => {
  return async (dispatch) => {
    dispatch({
      type: 'SET_TO_FULL'
    });
  };
};

export const setBoxToEmpty = () => {
  return async (dispatch) => {
    dispatch({
      type: 'SET_TO_EMPTY'
    });
  };
};

export default boxReducer;