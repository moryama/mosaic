import pieceService from '../services/pieceService';


const pieceReducer = (state = [], action) => {
  switch (action.type) {
  case 'INIT_PIECES':
    return action.pieces;
  case 'UPDATE_POSITION':
    return state.map(function (item) {
      return item.id !== action.data.id
        ? item
        : {
          ...item,
          loc: action.data.loc,
          top: action.data.top,
          left: action.data.left
        };
    });
  case 'SAVE_PIECES':
    return action.savedPieces;
  case 'RESET_PIECES':
    return action.initialPieces;
  default:
    return state;
  }
};

export const initializePieces = () => {
  return async (dispatch) => {
    const pieces = await pieceService.getAll();
    dispatch({
      type: 'INIT_PIECES',
      pieces
    });
  };
};

export const updatePosition = (item) => {
  return async (dispatch) => {
    dispatch({
      type: 'UPDATE_POSITION',
      data: {
        id: item.id,
        loc: item.loc,
        top: item.top,
        left: item.left
      }
    });
  };
};


export const savePieces = (pieces) => {
  return async (dispatch) => {
    const promiseToUpdateEachPiece = pieces.map(piece =>
      pieceService.update(piece.id, piece));
    const savedPieces = await Promise.all(promiseToUpdateEachPiece);
    dispatch({
      type: 'SAVE_PIECES',
      savedPieces
    });
  };
};


export const resetPieces = () => {
  return async (dispatch) => {
    const pieces = await pieceService.getInitial();
    const promiseToUpdateEachPiece = pieces.map(piece =>
      pieceService.update(piece.id, piece)
    );
    const initialPieces = await Promise.all(promiseToUpdateEachPiece);
    dispatch({
      type: 'RESET_PIECES',
      initialPieces
    });
  };
};

export default pieceReducer;