import { createStore, combineReducers, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import boxReducer from './reducers/boxReducer';
import pieceReducer from './reducers/pieceReducer';


const reducer = combineReducers({
  pieces: pieceReducer,
  boxIsFull: boxReducer
});

const store = createStore(
  reducer,
  applyMiddleware(thunk)
);

export default store;