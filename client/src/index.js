import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { DndProvider } from 'react-dnd';
import { HTML5Backend } from 'react-dnd-html5-backend';
import { BrowserView, MobileView } from "react-device-detect";
import App from './App';
import MobileMessage from './components/MobileMessage'
import store from './store';
import 'bootstrap/dist/css/bootstrap.min.css';



ReactDOM.render(
  <Provider store={store}>
    <DndProvider backend={HTML5Backend}>
      <React.StrictMode>
        <BrowserView>
          <App />
        </BrowserView>
        <MobileView>
          <MobileMessage />
        </MobileView>
      </React.StrictMode>
    </DndProvider>
  </Provider>,
  document.getElementById('root')
);
