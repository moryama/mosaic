import React from 'react';
import { useState } from 'react';
import { CountdownCircleTimer } from 'react-countdown-circle-timer';
import Button from 'react-bootstrap/Button';


const Timer = () => {
  const [restartKey, setRestartKey] = useState(0);
  const [isPlaying, setIsPlaying] = useState(false);

  const startTimer = () => {
    setIsPlaying(true);
    setRestartKey(prevKey => prevKey + 1);
  };

  return (
    <div>
      <div>
        <CountdownCircleTimer
          key={restartKey}
          isPlaying={isPlaying}
          duration={5}
          onComplete={() => {
            setIsPlaying(false);
            return [true];
          }}
          colors={[['#726a95', 0.25], ['#709fb0', 0.25], ['#a0c1b8', 0.25], ['#f4ebc1']]}
        >
          <Button onClick={() => startTimer()} variant="light">▷</Button>
        </CountdownCircleTimer>
      </div>

    </div>
  );
};

export default Timer;