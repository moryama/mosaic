import React from 'react';
import { useDrag } from 'react-dnd';
import { useDispatch, useSelector } from 'react-redux';
import { CONTAINER_NAMES, ITEM_TYPES } from '../constants';
import { updatePosition } from '../reducers/pieceReducer';
import { setBoxToEmpty } from '../reducers/boxReducer';

const Piece = ({ piece }) => {
  const dispatch = useDispatch();
  const isDraggingFromBox = useSelector(({ boxIsFull }) => {return boxIsFull;});
  const boxIsFull = useSelector(({ boxIsFull }) => { return boxIsFull; });

  // Set up dragging functionality
  const [{ isDragging }, dragRef] = useDrag({
    // define data being dragged
    item: {
      type: ITEM_TYPES.PIECE,
      id: piece.id,
      text: piece.text,
      top: piece.top,
      left: piece.left,
      loc: piece.loc
    },
    // set dragging restrictions
    canDrag: () => {
      return !(piece.loc === CONTAINER_NAMES.DESK && boxIsFull) &&
      !(piece.loc === CONTAINER_NAMES.HANGER && boxIsFull);
    },
    // set what happens after release
    end: (item, monitor) => {
      const didDrop = monitor.didDrop();
      if (isDraggingFromBox && didDrop) {
        dispatch(setBoxToEmpty());
      }

      const dropResult = monitor.getDropResult(); // get the new position
      if (dropResult) {
        const newLocation = dropResult.container;
        const newTop = dropResult.top;
        const newLeft = dropResult.left;
        dispatch(updatePosition({
          id: item.id,
          text: item.text,
          top: newTop,
          left: newLeft,
          loc: newLocation
        }));
      }
    },
    // get status to use for style
    collect: (monitor) => ({
      isDragging: monitor.isDragging()
    })

  });

  const PieceStyle = {
    position: 'absolute',
    border: '2px solid #AFB7BE',
    borderRadius: '5px',
    padding: '0.5rem 1rem',
    display: isDragging ? 'none' : '',
    cursor: 'move',
    top: piece.top,
    left: piece.left,
    color: 'black'
  };

  return (
    <span
      ref={dragRef}
      style={PieceStyle}>
      {piece.text}
    </span>
  );
};

export default Piece;