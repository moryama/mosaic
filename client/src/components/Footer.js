import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { resetPieces, savePieces } from '../reducers/pieceReducer';
import Button from 'react-bootstrap/Button';
import DropdownButton from 'react-bootstrap/DropdownButton';
import DropdownItem from 'react-bootstrap/esm/DropdownItem';
import FreddyImg from '../img/mercury.jpeg';


const Footer = () => {
  const dispatch = useDispatch();
  const currPieces = useSelector(({ pieces }) => {
    return pieces;
  });
  const saveGame = () => {
    dispatch(savePieces(currPieces));
  };

  const resetGame = () => {
    if (window.confirm('Are you sure?')) {
      dispatch(resetPieces());
    }
  };
  return (
    <footer className="footer container-fluid">
      <DropdownButton
        drop='up'
        title='about'
        variant="secondary"
      >
        <DropdownItem>
        This is a demo for a game interface used in online language classes.
          <br/>
        Objective of the game is to combine pieces and build a text on the main desk, top-left to bottom-right.
          <br/>
        The teacher controls the interface while sharing the screen with the students.
          <br/>
        Teams take turn, using the timer widget, moving pieces available from the box or the table.
          <br/>
        Points are assigned on the score board.
          <br/>
        In this example, the text to build is the intro of the Queen’s song ‘Bohemian Rhapsody’.
          <img src={FreddyImg} style={{ height: '5em' }} alt='Freddy Mercury meme'/>
        </DropdownItem>
      </DropdownButton>
      <Button onClick={() => resetGame()} variant='secondary'>reset</Button>
      <Button onClick={() => saveGame()} variant='secondary'>save</Button>
    </footer>
  );
};

export default Footer;