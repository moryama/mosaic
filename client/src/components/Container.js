import React from 'react';
import { useDrop } from 'react-dnd';
import { ITEM_TYPES } from '../constants';
import coordinates from '../utils/coordinates';


const Container = ({ style, containerName, children }) => {
  // Set up dropping functionality
  const [, dropRef] = useDrop({
    // define data being accepted
    accept: ITEM_TYPES.PIECE,
    // set what happens after item is dropped
    drop: (item, monitor) => {
      const { top, left } = coordinates.getNew(item, containerName, monitor);
      // pass data to the dragger (via dropResult)
      return({
        container: containerName,
        top: top,
        left: left
      });
    }
  });

  return (
    <div
      id={containerName}
      className="container"
      ref={dropRef}
      style={style}>
      {containerName === 'desk' ? 'MAIN DESK' : 'TABLE'}
      {children}
    </div>
  );
};

export default Container;