import React from 'react';


const MobileMessage = () => {

    const MobileMessageStyle = {
        marginTop: '3em',
        textAlign: 'center',
        font: 'Arial'
    };

    return (
        <div style={MobileMessageStyle}>
            <h3>This game is currently unavailable on mobile</h3>
            <h1>🐣</h1>
            <p>Ok...</p>
            <p>- take me to the <a href="https://gitlab.com/moryama/mosaic">source code</a> of the app</p>
            <p>- take me to the <a href="https://moryama.gitlab.io">developer's site</a></p>
            <p>- show me some <a href="https://old.reddit.com/r/LazyCats/">lazy cats</a></p>
        </div>
    );
};

export default MobileMessage;