import React, { useEffect, useState } from 'react';
import { useSelector } from 'react-redux';
import Button from 'react-bootstrap/Button';
import scoreService from '../services/scoreService';


const ScoreStyle = {
  height: '100%',
  border: '6px solid #f4ebc1',
  textAlign: 'center',
  color: '#574C87',
  fontWeight: 'bold',
};

const TeamScore = ({ teamName }) => {
  const [score, setScore] = useState(0);

  const maxScore = useSelector(({ pieces }) => {
    const piecesInQueue = pieces.filter(p => p.loc === 'queue');
    const numPieces = piecesInQueue.length;
    return Math.floor((((numPieces * 2) - (numPieces)) / 2) + numPieces);
  });

  useEffect(async () => {
    const currScore = await scoreService.get();
    const teamCurrScore = currScore.find(score => score.id === teamName);
    setScore(teamCurrScore.points);
  }, []);

  const updateScore = async (newScore) => {
    await scoreService.update(teamName, {
      points: newScore
    });
    setScore(newScore);
  };

  return (
    <div>
      <div style={{ marginTop: '1.2em' }}>
        <p>{teamName}:  {score} / {maxScore}</p>
        <Button onClick={() => updateScore(score + 1)} variant="light">+ 1</Button>
        <Button onClick={() => updateScore(score + 2)} variant="light">+ 2</Button>
        <Button onClick={() => updateScore(score - 1)} variant="light">⟲</Button>
      </div>
    </div>
  );
};

const Score = () => {
  return (
    <div className="container" style={ScoreStyle}>
      <p style={{ fontSize: '1.2em' }}>SCORE</p>
      <TeamScore teamName={'TEAM A'} />
      <TeamScore teamName={'TEAM B'} />
    </div>
  );
};

export default Score;