import React, { useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import Piece from './Piece';
import { setBoxToFull } from '../reducers/boxReducer';
import { updatePosition } from '../reducers/pieceReducer';


const BoxStyle = {
  width: '15em',
  height: '10em',
  border: '6px solid #709fb0',
  textAlign: 'center',
  color: '#AFB7BE'
};

const Box = () => {
  const [currentPiece, setCurrentPiece] = useState('');
  const boxIsFull = useSelector(({ boxIsFull }) => { return boxIsFull; });
  const dispatch = useDispatch();

  const piecesInQueue = useSelector(({ pieces }) => {
    return pieces.filter(piece => piece.loc === 'queue');
  });

  const pickRandomPieceFromQueue = (piecesInQueue) => {
    const piecesIds = piecesInQueue.map(piece => {
      return piece.id;
    });
    const randomId = Math.floor(Math.random() * ((piecesIds.length) - 0) + 0);
    return piecesInQueue[randomId];
  };

  const showRandomPiece = () => {
    if (boxIsFull) {
      return undefined;
    }
    const piece = pickRandomPieceFromQueue(piecesInQueue);
    if (piece) {
      dispatch(updatePosition({
        id: piece.id,
        text: piece.text,
        top: piece.top,
        left: piece.left,
        loc: 'box'
      }));
      dispatch(setBoxToFull());
      setCurrentPiece(piece);
    }
    return undefined;
  };

  return (
    <div
      className="container"
      style={BoxStyle}
      onClick={() => showRandomPiece()}>
      {
        piecesInQueue.length === 0 && !boxIsFull
          ? 'THE BOX IS EMPTY!'
          : 'BOX'
      }
      {
        boxIsFull
          ? <Piece piece={currentPiece} />
          : ''
      }
    </div>
  );
};

export default Box;