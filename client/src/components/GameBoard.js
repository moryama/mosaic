import React from 'react';
import Container from './Container';
import Piece from './Piece';
import { useSelector } from 'react-redux';
import { CONTAINER_NAMES } from '../constants';
import Box from './Box';

import BSContainer from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';


const DeskStyle = {
  height: '100%',
  border: '6px solid #a0c1b8',
  textAlign: 'center',
  color: '#AFB7BE'
};

const HangerStyle = {
  width: '15em',
  height: '20em',
  border: '6px solid #726a95',
  marginBottom: '1.3em',
  textAlign: 'center',
  color: '#AFB7BE'
};


const GameBoard = () => {

  const pieces = useSelector(({ pieces }) => {
    return pieces;
  });

  const returnPiecesForContainer = (containerName) => {
    return pieces
      .filter(piece => piece.loc === containerName)
      .map(piece => (
        <Piece
          key={piece.id}
          piece={piece}
        />
      ));
  };

  const { DESK, HANGER } = CONTAINER_NAMES;

  return (
    <div>
      <BSContainer>
        <Row>
          <Col lg={8}>
            <Container
              style={DeskStyle}
              containerName={DESK}
            >
              {returnPiecesForContainer(DESK)}
            </Container>
          </Col>
          <Col lg={4}>
            <Row>
              <Col>
                <Container
                  style={HangerStyle}
                  containerName={HANGER}
                >
                  {returnPiecesForContainer(HANGER)}
                </Container>
              </Col>
              <Col>
                <Box />
              </Col>
            </Row>
          </Col>
        </Row>
      </BSContainer>
    </div>
  );
};

export default GameBoard;