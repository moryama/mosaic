import React from 'react';
import Timer from './Timer';
import Score from './Score';
import BSContainer from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';


const Toolbar = () => {
  return (
    <div>
      <BSContainer>
        <Row className="justify-content-md-center"><Timer /></Row>
        <Row style={{ height:'4.4em' }}></Row>
        <Row className="justify-content-md-center"><Score /></Row>
      </BSContainer>
    </div>
  );
};


export default Toolbar;